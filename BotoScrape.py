"""
This is the template server side for ChatBot
"""
from bottle import route, run, template, static_file, request, Bottle
import json
import random
import pandas as pd
import os
import pickle
from copy import deepcopy


yes_list = ['y', "ye", "ys", "s", "es", 'yes', 'yyes', 'yees', 'yyees', 'yye', 'yee', 'yss', 'yeah', 'ok', 'ya', 'of',
            'k', 'course']
list_input = []
counter = 0
df = None
res = None
res_len = 0
price_max = 0
price_min = 0

# TODO REMOVE
path_hugo = r"C:\Users\hugoh\PycharmProjects\Entrainement_test2\chatbot-master3\chatbot-master\final_data.pkl"
with open(path_hugo, 'rb') as f:
    data_enriched = pickle.load(f)


def bot_message(input):
    global res
    global res_len
    global counter
    global super_counter

    res_len = 0
    if counter == 0:
        counter += 1
        return arrondissement_dynamic(input), "welcomefun"
    elif counter == 1:
        counter += 1
        return arrondissement_luxuary(input), "okfun"
    elif counter == 2:
        counter += 1
        return facilities(input), "okfun"
    elif counter == 3:
        counter += 1
        return shops(input), "okfun"
    elif counter == 4:
        counter += 1
        return prices_app(input), "okfun"
    elif counter == 5:
        counter += 1
        return "Let me see... ", "ngolo"
        # return "Here our selection for you: " + "http://localhost:8080/" + "\n are you ready to be impress?", "ngolo"
    elif counter == 6:
        counter += 1
        price = float(list_input[5])
        price_max = price * 1.15
        price_min = price * 0.85
        print(price_max, price_min)

        res = data_enriched

        i = 1
        if list_input[i] in yes_list:
            res = res[res['dynamic'] == True]

        i = 2
        if list_input[i] in yes_list:
            res = res[res['luxury'] == True]

        i = 3
        if list_input[i] in yes_list:
            res = res[res['facilities'] == True]

        i = 4
        if list_input[i] in yes_list:
            res = res[res['shops'] == True]

        i = 5
        res = res[res['rent'] <= price_max]
        res = res[res['rent'] >= price_min]

        i = 6
        res = res[res['Interesting'] == True]
        res_len = len(res)
        super_counter = -1
        # run(app, host='localhost', port=8080, debug=True)
        if res_len > 0:
            return "I've found " + str(res_len) + " results, do you want to see them buddy?", "ngolo"
        else:
            return "sorry buddy, there is no apartment with your criteria... :(", "crying"

    elif counter >= 7:
        if list_input[counter-1] in yes_list:
            counter += 1
            super_counter += 1
            print(super_counter)
            return res.loc[res.index.values.tolist()[super_counter], 'description'] + ('-' * 87) +\
                   "Do you want to see more ?", 'ngolo'
        else:
            return "Thanks for your visit, see you soon", "ngolo"


def arrondissement_dynamic(input):
    greeting_list = ["Nice to meet you {0}!", "Hello {0}, what's up?", "Hi {0}!"]
    question1 = "Do you want a dynamic arrondissement?"
    greeting = random.choice(greeting_list) + " I'am your personal real estate.\n" + question1
    return greeting.format(input.lower().split()[0].title())


def arrondissement_luxuary(input):
    question2 = "Do you want a luxuary arrondissement?"
    return question2.format(input.lower().split()[0].title())


def facilities(input):
    question3 = "... with many facilities?"
    return question3.format(input.lower().split()[0].title())


def shops(input):
    question4 = "... with many shops?"
    return question4.format(input.lower().split()[0].title())


def prices_app(input):
    question5 = "which price...?"
    return question5.format(input)


@route('/', method='GET')
def index():
    return template("chatbot.html")


@route("/chat", method='POST')
def chat():
    user_message = request.POST.get('msg')
    (user_message, animation) = bot_message(user_message)
    list_input.append(request.POST.get('msg'))
    print(list_input)
    return json.dumps({"animation": animation, "msg": user_message})


@route("/test", method='POST')
def chat():
    user_message = request.POST.get('msg')
    return json.dumps({"animation": "inlove", "msg": user_message})


@route('/js/<filename:re:.*\.js>', method='GET')
def javascripts(filename):
    return static_file(filename, root='js')


@route('/css/<filename:re:.*\.css>', method='GET')
def stylesheets(filename):
    return static_file(filename, root='css')


@route('/images/<filename:re:.*\.(jpg|png|gif|ico)>', method='GET')
def images(filename):
    return static_file(filename, root='images')


def main():
    run(host='localhost', port=7000)


"""bottle pr afficher dataframe final"""
# TODO
# app = Bottle()
#
#
# @app.route("/")
# def index():
#     df = make_df()
#     return df.to_html()


def make_df(list_input, data_enriched = data_enriched):
    res = data_enriched

    i = 1
    if list_input[i] in yes_list:
        res = res[res['dynamic'] == True]

    i = 2
    if list_input[i] in yes_list:
        res = res[res['luxury'] == True]

    i = 3
    if list_input[i] in yes_list:
        res = res[res['facilities'] == True]

    i = 4
    if list_input[i] in yes_list:
        res = res[res['shops'] == True]

    i = 5
    res = res[res['rent'] <= price_max]
    res = res[res['rent'] >= price_min]

    i = 6
    res = res[res['Interesting'] == True]
    return res, len(res)


"""fin bottle pr afficher dataframe final"""


if __name__ == '__main__':
    main()
